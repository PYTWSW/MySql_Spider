import pymysql

#连接MySQL数据库
connection = pymysql.connect(host = '127.0.0.1', port = 3306,
                             user = 'root', password = 'pyt19980801',
                             db = 'test', charset = 'utf8mb4',
                             cursorclass = pymysql.cursors.DictCursor)
#通过cursor创建游标
cursor = connection.cursor()


#创建SQL语句
sql = "UPDATE users SET Age = '17' WHERE Name = 'Liu'"

#执行SQL语句
cursor.execute(sql)

#提交SQL
connection.commit()

#关闭数据库连接和游标，防止数据泄漏
connection.close()
cursor.close()
