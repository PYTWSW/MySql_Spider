import pymysql

#连接MySQL数据库
connection = pymysql.connect(host = '127.0.0.1', port = 3306,
                             user = 'root', password = 'pyt19980801',
                             db = 'test', charset = 'utf8mb4',
                             cursorclass = pymysql.cursors.DictCursor)
#通过cursor创建游标
cursor = connection.cursor()


#创建查询SQL语句，查询单条数据
sql = "SELECT * FROM users WHERE Age = '19'"

cursor.execute(sql)

result = cursor.fetchone()
print(result)


'''
#创建查询SQL语句，查询多条数据
sql = "SELECT * FROM users"

cursor.execute(sql)

result = cursor.fetchall()
for data in result:
    print(data)
'''

#关闭数据库连接和游标，防止数据泄漏
connection.close()
cursor.close()